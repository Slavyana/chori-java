package com.example.chori.model;

import com.example.chori.dto.ResponseFile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

/**
 * The type Employee.
 */
@Document(collection = "employees")
public class Employee {
    @Id
    private String id;

    private String firstName;
    private String lastName;

    private Date dateOfBirth;

    private List<DateSlots> dateSlotsList;

    private List<String> services;

    private ResponseFile profileImage;

    @Valid
    private Business business;

    @NotBlank
    @Indexed(unique = true)
    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9]+$")
    private String username;


    /**
     * Instantiates a new Employee.
     */
    public Employee() {
    }

    /**
     * Instantiates a new Employee.
     *
     * @param firstName the first name
     * @param lastName  the last name
     */
    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets date slots list.
     *
     * @return the date slots list
     */
    public List<DateSlots> getDateSlotsList() {
        return dateSlotsList;
    }

    /**
     * Sets date slots list.
     *
     * @param dateSlotsList the date slots list
     */
    public void setDateSlotsList(List<DateSlots> dateSlotsList) {
        this.dateSlotsList = dateSlotsList;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public Business getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(Business business) {
        this.business = business;
    }

    /**
     * Gets services.
     *
     * @return the services
     */
    public List<String> getServices() {
        return services;
    }

    /**
     * Sets services.
     *
     * @param services the services
     */
    public void setServices(List<String> services) {
        this.services = services;
    }

    /**
     * Gets profile image.
     *
     * @return the profile image
     */
    public ResponseFile getProfileImage() {
        return profileImage;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Sets profile image.
     *
     * @param profileImage the profile image
     */
    public void setProfileImage(ResponseFile profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * Gets date of birth.
     *
     * @return the date of birth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets date of birth.
     *
     * @param dateOfBirth the date of birth
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
