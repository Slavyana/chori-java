package com.example.chori.model;

import org.springframework.data.annotation.Id;


import org.springframework.data.mongodb.core.mapping.Document;

/**
 * The type File.
 */
@Document(collection = "files")
public class File {
    @Id
    private String id;

    private String name;

    private String type;

    private byte[] data;

    /**
     * Instantiates a new File.
     */
    public File() {
    }

    /**
     * Instantiates a new File.
     *
     * @param name the name
     * @param type the type
     * @param data the data
     */
    public File(String name, String type, byte[] data) {
        this.name = name;
        this.type = type;
        this.data = data;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get data byte [ ].
     *
     * @return the byte [ ]
     */
    public byte[] getData() {
        return data;
    }

    /**
     * Sets data.
     *
     * @param data the data
     */
    public void setData(byte[] data) {
        this.data = data;
    }

}
