package com.example.chori.model;

import com.example.chori.dto.ResponseFile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * The type Business.
 */
@Document(collection = "businesses")
public class Business {
    @Id
    private String id;

    @NotBlank
    @Indexed(unique = true)
    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String username;

    private String description;

    @NotBlank
    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String name;

    @NotBlank
    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String address;

    private ResponseFile profileImage;

    @NotBlank
    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String country;

    @NotBlank
    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String region;

    /**
     * Instantiates a new Business.
     */
    public Business() {
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets profile image.
     *
     * @return the profile image
     */
    public ResponseFile getProfileImage() {
        return profileImage;
    }

    /**
     * Sets profile image.
     *
     * @param profileImage the profile image
     */
    public void setProfileImage(ResponseFile profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets region.
     *
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets region.
     *
     * @param region the region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
