package com.example.chori.model;

import java.util.Date;
import java.util.List;

/**
 * The type Date slots.
 */
public class DateSlots {
    private Date date;

    private List<Integer> slots;


    /**
     * Instantiates a new Date slots.
     *
     * @param date  the date
     * @param slots the slots
     */
    public DateSlots(Date date, List<Integer> slots) {
        this.date = date;
        this.slots = slots;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets slots.
     *
     * @return the slots
     */
    public List<Integer> getSlots() {
        return slots;
    }

    /**
     * Sets slots.
     *
     * @param slots the slots
     */
    public void setSlots(List<Integer> slots) {
        this.slots = slots;
    }
}
