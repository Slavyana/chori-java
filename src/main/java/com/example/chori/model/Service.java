package com.example.chori.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * The type Service.
 */
@Document(collection = "services")
public class Service {
    @Id
    private String id;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String name;
    private String description;
    private Integer durationInMinutes;

    private String currency;

    private Double price;

    private List<ObjectId> employees;

    private String business;

    /**
     * Instantiates a new Service.
     */
    public Service() {
    }

    /**
     * Instantiates a new Service.
     *
     * @param id          the id
     * @param name        the name
     * @param description the description
     * @param duration    the duration
     * @param employees   the employees
     */
    public Service(String id, String name, String description, Integer duration, List<Employee> employees) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.durationInMinutes = duration;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets duration in minutes.
     *
     * @return the duration in minutes
     */
    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    /**
     * Sets duration in minutes.
     *
     * @param durationInMinutes the duration in minutes
     */
    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public List<ObjectId> getEmployees() {
        return employees;
    }

    /**
     * Sets employees.
     *
     * @param employees the employees
     */
    public void setEmployees(List<ObjectId> employees) {
        this.employees = employees;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public String getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(String business) {
        this.business = business;
    }

    /**
     * Gets currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets currency.
     *
     * @param currency the currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
