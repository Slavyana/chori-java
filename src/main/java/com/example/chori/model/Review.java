package com.example.chori.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * The type Review.
 */
@Document(collection = "reviews")
public class Review {
    @Id
    private String id;

    private Client author;

    private Business business;

    private Boolean isAnonymous;

    private Integer rating;

    private String content;

    /**
     * Instantiates a new Review.
     */
    public Review() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public Client getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(Client author) {
        this.author = author;
    }

    /**
     * Gets rating.
     *
     * @return the rating
     */
    public Integer getRating() {
        return rating;
    }

    /**
     * Sets rating.
     *
     * @param rating the rating
     */
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public Business getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(Business business) {
        this.business = business;
    }

    /**
     * Gets is anonymous.
     *
     * @return the is anonymous
     */
    public Boolean getIsAnonymous() {
        return isAnonymous;
    }

    /**
     * Sets is anonymous.
     *
     * @param anonymous the anonymous
     */
    public void setIsAnonymous(Boolean anonymous) {
        isAnonymous = anonymous;
    }
}
