package com.example.chori.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * The type Comment.
 */
@Document(collection = "comments")
public class Comment {
    @Id
    private String id;

    private User author;

    private String content;

    private Set<User> likes = new HashSet<User>();

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Gets likes.
     *
     * @return the likes
     */
    public Set<User> getLikes() {
        return likes;
    }

    /**
     * Sets likes.
     *
     * @param likes the likes
     */
    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    /**
     * Like.
     *
     * @param user the user
     */
    public void like(User user) {
        Optional<User> existingUser = likes.stream().filter(u -> u.getId().equals(user.getId())).findFirst();

        if (existingUser.isEmpty()) {
            likes.add(user);
        }
    }

    /**
     * Unlike.
     *
     * @param userId the user id
     */
    public void unlike(String userId) {
        Optional<User> like = likes.stream().filter(u -> u.getId().equals(userId)).findFirst();
        like.ifPresent(user -> likes.remove(user));
    }
}
