package com.example.chori.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * The type Appointment.
 */
@Document(collection = "appointments")
public class Appointment {
    @Id
    private String id;

    private Employee employee;

    private Business business;

    private DateSlots dateSlots;

    private Service service;

    private Client client;


    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endTime;

    /**
     * Instantiates a new Appointment.
     */
    public Appointment() {
    }

    /**
     * Instantiates a new Appointment.
     *
     * @param id        the id
     * @param employee  the employee
     * @param business  the business
     * @param dateSlots the date slots
     * @param service   the service
     * @param client    the client
     * @param startTime the start time
     * @param endTime   the end time
     */
    public Appointment(String id, Employee employee, Business business, DateSlots dateSlots, Service service, Client client, LocalDateTime startTime, LocalDateTime endTime) {
        this.id = id;
        this.employee = employee;
        this.business = business;
        this.dateSlots = dateSlots;
        this.service = service;
        this.client = client;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets employee.
     *
     * @return the employee
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * Sets employee.
     *
     * @param employee the employee
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public Business getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(Business business) {
        this.business = business;
    }

    /**
     * Gets date slots.
     *
     * @return the date slots
     */
    public DateSlots getDateSlots() {
        return dateSlots;
    }

    /**
     * Sets date slots.
     *
     * @param dateSlots the date slots
     */
    public void setDateSlots(DateSlots dateSlots) {
        this.dateSlots = dateSlots;
    }

    /**
     * Gets service.
     *
     * @return the service
     */
    public Service getService() {
        return service;
    }

    /**
     * Sets service.
     *
     * @param service the service
     */
    public void setService(Service service) {
        this.service = service;
    }

    /**
     * Gets start time.
     *
     * @return the start time
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * Sets start time.
     *
     * @param startTime the start time
     */
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets end time.
     *
     * @return the end time
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * Sets end time.
     *
     * @param endTime the end time
     */
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(Client client) {
        this.client = client;
    }
}
