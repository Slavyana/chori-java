package com.example.chori.model;

import com.example.chori.dto.ResponseFile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Pattern;

/**
 * The type Product.
 */
@Document(collection = "products")
public class Product {
    @Id
    private String id;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String name;
    private String description;
    private Double price;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}]+$")
    private String currency;

    private Business business;

    private ResponseFile image;


    /**
     * Instantiates a new Product.
     */
    public Product() {
    }

    /**
     * Instantiates a new Product.
     *
     * @param id          the id
     * @param name        the name
     * @param description the description
     * @param price       the price
     * @param currency    the currency
     * @param business    the business
     * @param image       the image
     */
    public Product(String id, String name, String description, Double price, String currency, Business business, ResponseFile image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.currency = currency;
        this.business = business;
        this.image = image;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public Business getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(Business business) {
        this.business = business;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public ResponseFile getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(ResponseFile image) {
        this.image = image;
    }

    /**
     * Gets currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets currency.
     *
     * @param currency the currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
