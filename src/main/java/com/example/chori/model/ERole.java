package com.example.chori.model;

/**
 * The enum E role.
 */
public enum ERole {
    /**
     * Role user e role.
     */
    ROLE_USER,
    /**
     * Role moderator e role.
     */
    ROLE_MODERATOR,
    /**
     * Role admin e role.
     */
    ROLE_ADMIN,
    /**
     * Role employee e role.
     */
    ROLE_EMPLOYEE,
    /**
     * Role manager e role.
     */
    ROLE_MANAGER
}