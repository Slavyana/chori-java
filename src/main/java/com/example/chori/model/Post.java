package com.example.chori.model;

import com.example.chori.dto.ResponseFile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

/**
 * The type Post.
 */
@Document(collection = "posts")
public class Post {
    @Id
    private String id;

    private User author;

    private String description;

    private ResponseFile image;

    private List<Service> services;

    private Business business;

    private List<Comment> comments = new ArrayList<Comment>();

    private Set<User> likes = new HashSet<>();

    /**
     * Instantiates a new Post.
     */
    public Post() {

    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public ResponseFile getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(ResponseFile image) {
        this.image = image;
    }

    /**
     * Gets services.
     *
     * @return the services
     */
    public List<Service> getServices() {
        return services;
    }

    /**
     * Sets services.
     *
     * @param services the services
     */
    public void setServices(List<Service> services) {
        this.services = services;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public Business getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(Business business) {
        this.business = business;
    }

    /**
     * Gets comments.
     *
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * Sets comments.
     *
     * @param comments the comments
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * Gets likes.
     *
     * @return the likes
     */
    public Set<User> getLikes() {
        return likes;
    }

    /**
     * Sets likes.
     *
     * @param likes the likes
     */
    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * Add comment.
     *
     * @param comment the comment
     */
    public void addComment(Comment comment) {
        comments.add(0, comment);
    }

    /**
     * Edit comment.
     *
     * @param comment the comment
     */
    public void editComment(Comment comment) {
        if (comments == null) return;
        Optional<Comment> oldComment = comments.stream().filter(c -> c.getId().equals(comment.getId())).findFirst();
        if (oldComment.isPresent()) {
            int index = comments.indexOf(oldComment.get());
            comments.set(index, comment);
        }
    }

    /**
     * Delete comment.
     *
     * @param commentId the comment id
     */
    public void deleteComment(String commentId) {
        if (comments == null) return;
        Optional<Comment> oldComment = comments.stream().filter(c -> c.getId().equals(commentId)).findFirst();
        oldComment.ifPresent(comment -> comments.remove(comment));
    }

    /**
     * Like.
     *
     * @param user the user
     */
    public void like(User user) {

        Optional<User> existingUser = likes.stream().filter(u -> u.getId().equals(user.getId())).findFirst();

        if (existingUser.isEmpty()) {
            likes.add(user);
        }
    }

    /**
     * Unlike.
     *
     * @param userId the user id
     */
    public void unlike(String userId) {
        Optional<User> like = likes.stream().filter(u -> u.getId().equals(userId)).findFirst();
        like.ifPresent(user -> likes.remove(user));
    }
}
