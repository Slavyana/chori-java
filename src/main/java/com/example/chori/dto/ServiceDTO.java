package com.example.chori.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * The type Service dto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder("id")
public class ServiceDTO {
    private String id;

    @NotBlank
    @Pattern(message = "Name can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String name;

    private String description;
    private Integer durationInMinutes;
    private Double price;

    @NotBlank
    @Pattern(message = "Currency can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String currency;

    private String business;

    private List<ObjectId> employees;

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets duration in minutes.
     *
     * @return the duration in minutes
     */
    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    /**
     * Sets duration in minutes.
     *
     * @param durationInMinutes the duration in minutes
     */
    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }


    /**
     * Gets employees.
     *
     * @return the employees
     */
    public List<ObjectId> getEmployees() {
        return employees;
    }

    /**
     * Sets employees.
     *
     * @param employees the employees
     */
    public void setEmployees(List<ObjectId> employees) {
        this.employees = employees;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public String getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(String business) {
        this.business = business;
    }

    /**
     * Gets currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets currency.
     *
     * @param currency the currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
