package com.example.chori.dto;


import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * The type Signup request.
 */
public class SignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private Set<String> roles;

    /**
     * The Employee.
     */
    EmployeeDTO employee;

    /**
     * The Client.
     */
    ClientDTO client;

    /**
     * The Business.
     */
    @Valid
    BusinessDTO business;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets roles.
     *
     * @return the roles
     */
    public Set<String> getRoles() {
        return this.roles;
    }

    /**
     * Sets role.
     *
     * @param roles the roles
     */
    public void setRole(Set<String> roles) {
        this.roles = roles;
    }

    /**
     * Gets employee.
     *
     * @return the employee
     */
    public EmployeeDTO getEmployee() {
        return employee;
    }

    /**
     * Sets employee.
     *
     * @param employee the employee
     */
    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public ClientDTO getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(ClientDTO client) {
        this.client = client;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public BusinessDTO getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(BusinessDTO business) {
        this.business = business;
    }
}