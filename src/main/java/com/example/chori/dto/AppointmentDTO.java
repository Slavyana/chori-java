package com.example.chori.dto;

import com.example.chori.model.DateSlots;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDateTime;

/**
 * The type Appointment dto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder("id")
public class AppointmentDTO {
    private String id;

    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private ServiceDTO service;

    private EmployeeDTO employee;

    private ClientDTO client;

    private BusinessDTO business;

    private DateSlots dateSlots;

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets start time.
     *
     * @return the start time
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * Sets start time.
     *
     * @param startTime the start time
     */
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets end time.
     *
     * @return the end time
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * Sets end time.
     *
     * @param endTime the end time
     */
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Gets service.
     *
     * @return the service
     */
    public ServiceDTO getService() {
        return service;
    }

    /**
     * Sets service.
     *
     * @param service the service
     */
    public void setService(ServiceDTO service) {
        this.service = service;
    }

    /**
     * Gets employee.
     *
     * @return the employee
     */
    public EmployeeDTO getEmployee() {
        return employee;
    }

    /**
     * Sets employee.
     *
     * @param employee the employee
     */
    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public BusinessDTO getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(BusinessDTO business) {
        this.business = business;
    }

    /**
     * Gets date slots.
     *
     * @return the date slots
     */
    public DateSlots getDateSlots() {
        return dateSlots;
    }

    /**
     * Sets date slots.
     *
     * @param dateSlots the date slots
     */
    public void setDateSlots(DateSlots dateSlots) {
        this.dateSlots = dateSlots;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public ClientDTO getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(ClientDTO client) {
        this.client = client;
    }
}
