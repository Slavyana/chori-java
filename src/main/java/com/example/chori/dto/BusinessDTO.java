package com.example.chori.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.Pattern;

/**
 * The type Business dto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder("id")
public class BusinessDTO {
    private String Id;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String name;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9]+$")
    private String username;

    private String description;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]*$")
    private String country;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]*$")
    private String region;

    @Pattern(message = "Type can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String address;

    private ResponseFile profileImage;

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return Id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        Id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets region.
     *
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets region.
     *
     * @param region the region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets profile image.
     *
     * @return the profile image
     */
    public ResponseFile getProfileImage() {
        return profileImage;
    }

    /**
     * Sets profile image.
     *
     * @param profileImage the profile image
     */
    public void setProfileImage(ResponseFile profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}