package com.example.chori.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

/**
 * The type Employee dto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder("id")
public class EmployeeDTO {

    private String Id;

    @NotBlank
    @Pattern(message = "First name can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String firstName;

    @NotBlank
    @Pattern(message = "Last name can contain alphanumeric characters only", regexp = "^[\\p{L}0-9 ]+$")
    private String lastName;
    private BusinessDTO business;
    private List<String> services;
    private String username;
    private ResponseFile profileImage;
    private Date dateOfBirth;

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return Id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        Id = id;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets services.
     *
     * @return the services
     */
    public List<String> getServices() {
        return services;
    }

    /**
     * Sets services.
     *
     * @param services the services
     */
    public void setServices(List<String> services) {
        this.services = services;
    }

    /**
     * Gets business.
     *
     * @return the business
     */
    public BusinessDTO getBusiness() {
        return business;
    }

    /**
     * Sets business.
     *
     * @param business the business
     */
    public void setBusiness(BusinessDTO business) {
        this.business = business;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets date of birth.
     *
     * @return the date of birth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets date of birth.
     *
     * @param dateOfBirth the date of birth
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets profile image.
     *
     * @return the profile image
     */
    public ResponseFile getProfileImage() {
        return profileImage;
    }

    /**
     * Sets profile image.
     *
     * @param responseFile the response file
     */
    public void setProfileImage(ResponseFile responseFile) {
        this.profileImage = responseFile;
    }
}
