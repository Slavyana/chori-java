package com.example.chori.dto.mapper;

import com.example.chori.dto.ProductDTO;
import com.example.chori.model.Product;

/**
 * The type Product mapper.
 */
public class ProductMapper {
    /**
     * To product dto product dto.
     *
     * @param product the product
     * @return the product dto
     */
    public static ProductDTO toProductDTO(Product product) {
        ProductDTO newProductDTO =  new ProductDTO();
        newProductDTO.setId(product.getId());
        newProductDTO.setName(product.getName());
        newProductDTO.setDescription(product.getDescription());
        newProductDTO.setPrice(product.getPrice());
        newProductDTO.setCurrency(product.getCurrency());
        newProductDTO.setImage(product.getImage());
        newProductDTO.setBusiness(product.getBusiness());

        return newProductDTO;
    }

    /**
     * To product product.
     *
     * @param productDTO the product dto
     * @return the product
     */
    public static Product toProduct(ProductDTO productDTO) {
        Product newProduct =  new Product();
        newProduct.setId(productDTO.getId());
        newProduct.setName(productDTO.getName());
        newProduct.setDescription(productDTO.getDescription());
        newProduct.setPrice(productDTO.getPrice());
        newProduct.setCurrency(productDTO.getCurrency());
        newProduct.setImage(productDTO.getImage());
        newProduct.setBusiness(productDTO.getBusiness());


        return newProduct;
    }
}
