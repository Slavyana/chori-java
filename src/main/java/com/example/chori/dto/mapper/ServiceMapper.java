package com.example.chori.dto.mapper;

import com.example.chori.dto.ServiceDTO;
import com.example.chori.model.Service;


/**
 * The type Service mapper.
 */
public class ServiceMapper {
    /**
     * To service dto service dto.
     *
     * @param service the service
     * @return the service dto
     */
    public static ServiceDTO toServiceDTO(Service service) {
        ServiceDTO newServiceDTO =  new ServiceDTO();
        newServiceDTO.setId(service.getId());
        newServiceDTO.setName(service.getName());
        newServiceDTO.setDescription(service.getDescription());
        newServiceDTO.setDurationInMinutes(service.getDurationInMinutes());
        newServiceDTO.setPrice(service.getPrice());
        newServiceDTO.setEmployees(service.getEmployees());
        newServiceDTO.setBusiness(service.getBusiness());
        newServiceDTO.setCurrency(service.getCurrency());

//        newBusinessDTO.setEmployees(business.getEmployees());

        return newServiceDTO;
    }

    /**
     * To service service.
     *
     * @param serviceDTO the service dto
     * @return the service
     */
    public static Service toService(ServiceDTO serviceDTO) {
        Service newService =  new Service();
        newService.setId(serviceDTO.getId());
        newService.setName(serviceDTO.getName());
        newService.setDescription(serviceDTO.getDescription());
        newService.setDurationInMinutes(serviceDTO.getDurationInMinutes());
        newService.setPrice(serviceDTO.getPrice());
        newService.setEmployees(serviceDTO.getEmployees());
       newService.setBusiness(serviceDTO.getBusiness());
        newService.setCurrency(serviceDTO.getCurrency());


        return newService;
    }

}
