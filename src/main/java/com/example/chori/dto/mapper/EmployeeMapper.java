package com.example.chori.dto.mapper;

import com.example.chori.dto.EmployeeDTO;
import com.example.chori.model.Employee;

/**
 * The type Employee mapper.
 */
public class EmployeeMapper {

    /**
     * To employee dto employee dto.
     *
     * @param employee the employee
     * @return the employee dto
     */
    public static EmployeeDTO toEmployeeDTO(Employee employee) {
        EmployeeDTO newEmployeeDTO =  new EmployeeDTO();
        newEmployeeDTO.setId(employee.getId());
        newEmployeeDTO.setFirstName(employee.getFirstName());
        newEmployeeDTO.setLastName(employee.getLastName());
        newEmployeeDTO.setUsername(employee.getUsername());
        newEmployeeDTO.setDateOfBirth(employee.getDateOfBirth());
        newEmployeeDTO.setProfileImage(employee.getProfileImage());


        newEmployeeDTO.setServices(employee.getServices());

        if(employee.getBusiness() != null)
            newEmployeeDTO.setBusiness(BusinessMapper.toBusinessDTO(employee.getBusiness()));
        return newEmployeeDTO;
    }

    /**
     * To employee employee.
     *
     * @param employeeDto the employee dto
     * @return the employee
     */
    public static Employee toEmployee(EmployeeDTO employeeDto) {
        Employee newEmployee =  new Employee();
        newEmployee.setId(employeeDto.getId());
        newEmployee.setFirstName(employeeDto.getFirstName());
        newEmployee.setLastName(employeeDto.getLastName());
        newEmployee.setUsername(employeeDto.getUsername());
        newEmployee.setDateOfBirth(employeeDto.getDateOfBirth());

        newEmployee.setProfileImage(employeeDto.getProfileImage());

        newEmployee.setServices(employeeDto.getServices());

        if(employeeDto.getBusiness() != null)
            newEmployee.setBusiness(BusinessMapper.toBusiness(employeeDto.getBusiness()));
        return newEmployee;
    }
}
