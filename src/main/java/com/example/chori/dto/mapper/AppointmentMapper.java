package com.example.chori.dto.mapper;


import com.example.chori.dto.AppointmentDTO;
import com.example.chori.model.Appointment;

/**
 * The type Appointment mapper.
 */
public class AppointmentMapper {

    /**
     * To appointment dto appointment dto.
     *
     * @param appointment the appointment
     * @return the appointment dto
     */
    public static AppointmentDTO toAppointmentDTO(Appointment appointment) {
        AppointmentDTO newAppointmentDTO =  new AppointmentDTO();
        newAppointmentDTO.setId(appointment.getId());
        newAppointmentDTO.setStartTime(appointment.getStartTime());
        newAppointmentDTO.setEndTime(appointment.getEndTime());
        newAppointmentDTO.setEmployee(EmployeeMapper.toEmployeeDTO(appointment.getEmployee()));
        newAppointmentDTO.setBusiness(BusinessMapper.toBusinessDTO(appointment.getBusiness()));
        newAppointmentDTO.setService(ServiceMapper.toServiceDTO(appointment.getService()));
        newAppointmentDTO.setDateSlots(appointment.getDateSlots());
        if(appointment.getClient() != null)
        newAppointmentDTO.setClient(ClientMapper.toClientDTO(appointment.getClient()));

        return newAppointmentDTO;
    }

    /**
     * To appointment appointment.
     *
     * @param appointmentDTO the appointment dto
     * @return the appointment
     */
    public static Appointment toAppointment(AppointmentDTO appointmentDTO) {
        Appointment newAppointment =  new Appointment();
        newAppointment.setId(appointmentDTO.getId());
        newAppointment.setStartTime(appointmentDTO.getStartTime());
        newAppointment.setEndTime(appointmentDTO.getEndTime());
        newAppointment.setEmployee(EmployeeMapper.toEmployee(appointmentDTO.getEmployee()));
        newAppointment.setBusiness(BusinessMapper.toBusiness(appointmentDTO.getBusiness()));
        newAppointment.setService(ServiceMapper.toService(appointmentDTO.getService()));
        newAppointment.setDateSlots(appointmentDTO.getDateSlots());
        if(appointmentDTO.getClient() != null)
        newAppointment.setClient(ClientMapper.toClient(appointmentDTO.getClient()));

        return newAppointment;
    }

}
