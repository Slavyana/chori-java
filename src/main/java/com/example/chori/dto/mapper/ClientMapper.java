package com.example.chori.dto.mapper;

import com.example.chori.dto.ClientDTO;
import com.example.chori.model.Client;

/**
 * The type Client mapper.
 */
public class ClientMapper {
    /**
     * To client dto client dto.
     *
     * @param client the client
     * @return the client dto
     */
    public static ClientDTO toClientDTO(Client client) {
        ClientDTO newClientDto =  new ClientDTO();
        newClientDto.setId(client.getId());
        newClientDto.setFirstName(client.getFirstName());
        newClientDto.setLastName(client.getLastName());
        newClientDto.setUsername(client.getUsername());
        newClientDto.setBio(client.getBio());
        newClientDto.setPhoneNumber(client.getPhoneNumber());
        newClientDto.setDateOfBirth(client.getDateOfBirth());
        newClientDto.setProfileImage(client.getProfileImage());


        return newClientDto;
    }

    /**
     * To client client.
     *
     * @param clientDto the client dto
     * @return the client
     */
    public static Client toClient(ClientDTO clientDto) {
        Client newClient =  new Client();
        newClient.setId(clientDto.getId());
        newClient.setFirstName(clientDto.getFirstName());
        newClient.setLastName(clientDto.getLastName());
        newClient.setUsername(clientDto.getUsername());
        newClient.setUsername(clientDto.getUsername());
        newClient.setBio(clientDto.getBio());
        newClient.setPhoneNumber(clientDto.getPhoneNumber());
        newClient.setDateOfBirth(clientDto.getDateOfBirth());
        newClient.setProfileImage(clientDto.getProfileImage());


        return newClient;
    }
}
