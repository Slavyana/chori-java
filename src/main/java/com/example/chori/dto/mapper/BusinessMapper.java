package com.example.chori.dto.mapper;

import com.example.chori.dto.BusinessDTO;
import com.example.chori.model.Business;

/**
 * The type Business mapper.
 */
public class BusinessMapper {
    /**
     * To business dto business dto.
     *
     * @param business the business
     * @return the business dto
     */
    public static BusinessDTO toBusinessDTO(Business business) {
        BusinessDTO newBusinessDTO =  new BusinessDTO();
        newBusinessDTO.setId(business.getId());
        newBusinessDTO.setUsername(business.getUsername());
        newBusinessDTO.setName(business.getName());
        newBusinessDTO.setAddress(business.getAddress());
        newBusinessDTO.setCountry(business.getCountry());
        newBusinessDTO.setRegion(business.getRegion());
        newBusinessDTO.setDescription(business.getDescription());
        newBusinessDTO.setProfileImage(business.getProfileImage());


        return newBusinessDTO;
    }

    /**
     * To business business.
     *
     * @param businessDTO the business dto
     * @return the business
     */
    public static Business toBusiness(BusinessDTO businessDTO) {
        Business newBusiness =  new Business();
        newBusiness.setId(businessDTO.getId());
        newBusiness.setUsername((businessDTO.getUsername()));
        newBusiness.setName(businessDTO.getName());
        newBusiness.setAddress(businessDTO.getAddress());
        newBusiness.setCountry(businessDTO.getCountry());
        newBusiness.setRegion(businessDTO.getRegion());
        newBusiness.setDescription(businessDTO.getDescription());

        newBusiness.setProfileImage(businessDTO.getProfileImage());

        return newBusiness;
    }
}
