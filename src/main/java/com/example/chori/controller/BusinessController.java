package com.example.chori.controller;

import com.example.chori.dto.*;
import com.example.chori.model.Post;
import com.example.chori.model.Review;
import com.example.chori.service.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Business controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/businesses")
public class BusinessController {

    private final BusinessService businessService;
    private final ServiceService serviceService;
    private final EmployeeService employeeService;
    private final AppointmentService appointmentService;
    private final ProductService productService;
    private final PostService postService;
    private final ReviewService reviewService;


    /**
     * Instantiates a new Business controller.
     *
     * @param businessService    the business service
     * @param serviceService     the service service
     * @param employeeService    the employee service
     * @param appointmentService the appointment service
     * @param productService     the product service
     * @param postService        the post service
     * @param reviewService      the review service
     */
    BusinessController(BusinessService businessService, ServiceService serviceService, EmployeeService employeeService, AppointmentService appointmentService, ProductService productService, PostService postService, ReviewService reviewService) {
        this.businessService = businessService;
        this.serviceService = serviceService;
        this.employeeService = employeeService;
        this.appointmentService = appointmentService;
        this.productService = productService;
        this.postService = postService;
        this.reviewService = reviewService;
    }

    /**
     * Gets all businesses.
     *
     * @param search      the search
     * @param country     the country
     * @param region      the region
     * @param serviceName the service name
     * @return the all businesses
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<BusinessDTO>> getAllBusinesses(@RequestParam(required = false) String search, @RequestParam(required = false) String country, @RequestParam(required = false) String region, @RequestParam(required = false) String serviceName) {

        List<BusinessDTO> result = businessService.getAllBusinesses();

        if (search != null && !search.isEmpty()) {
            return new ResponseEntity<>(businessService.getAllBusinessesBySearch(search), HttpStatus.OK);
        }
        if (country != null && !country.isEmpty() && (region == null || region.isEmpty())) {
            result = businessService.getAllBusinessesByCountry(country);
        } else if (country != null && !country.isEmpty()) {
            result = businessService.getAllBusinessesByCountryAndRegion(country, region);
        }

        if (serviceName != null && !serviceName.isEmpty()) {
            List<BusinessDTO> filteredResult = new ArrayList<>();
            for (BusinessDTO business : result) {
                List<ServiceDTO> services = serviceService.getServicesByBusiness(business.getId());
                if (services != null) {
                    List<ServiceDTO> filteredServices = services.stream().filter(s -> s.getName().toLowerCase().contains(serviceName.toLowerCase())).collect(Collectors.toList());
                    if (!filteredServices.isEmpty()) {
                        filteredResult.add(business);
                    }
                }
            }
            return new ResponseEntity<>(filteredResult, HttpStatus.OK);

        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Create business response entity.
     *
     * @param businessDTO the business dto
     * @return the response entity
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<BusinessDTO> createBusiness(@RequestBody @Valid BusinessDTO businessDTO) {
        return new ResponseEntity<>(businessService.createBusiness(businessDTO), HttpStatus.CREATED);
    }

    /**
     * Gets business by id.
     *
     * @param id the id
     * @return the business by id
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<BusinessDTO> getBusinessById(@PathVariable String id) {
        return new ResponseEntity<>(businessService.getBusinessById(id), HttpStatus.OK);
    }

    /**
     * Update business response entity.
     *
     * @param businessDTO the business dto
     * @param id          the id
     * @return the response entity
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<BusinessDTO> updateBusiness(@RequestBody @Valid BusinessDTO businessDTO, @PathVariable String id) {
        return new ResponseEntity<>(businessService.updateBusiness(businessDTO, id), HttpStatus.OK);
    }

    /**
     * Delete business response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<BusinessDTO> deleteBusiness(@PathVariable String id) {
        return new ResponseEntity<>(businessService.deleteBusinessById(id), HttpStatus.OK);
    }

    /**
     * Gets business services.
     *
     * @param id the id
     * @return the business services
     */
    @GetMapping("/{id}/services")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ServiceDTO>> getBusinessServices(@PathVariable String id) {
        return new ResponseEntity<>(serviceService.getServicesByBusiness(id), HttpStatus.OK);
    }

    /**
     * Gets business employees.
     *
     * @param id the id
     * @return the business employees
     */
    @GetMapping("/{id}/employees")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<EmployeeDTO>> getBusinessEmployees(@PathVariable String id) {
        return new ResponseEntity<>(employeeService.getEmployeesByBusiness(id), HttpStatus.OK);
    }

    /**
     * Gets business products.
     *
     * @param id the id
     * @return the business products
     */
    @GetMapping("/{id}/products")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductDTO>> getBusinessProducts(@PathVariable String id) {
        return new ResponseEntity<>(productService.getProductsByBusiness(id), HttpStatus.OK);
    }


    /**
     * Gets business posts.
     *
     * @param id the id
     * @return the business posts
     */
    @GetMapping("/{id}/posts")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Post>> getBusinessPosts(@PathVariable String id) {
        return new ResponseEntity<>(postService.getPostsByBusiness(id), HttpStatus.OK);
    }

    /**
     * Gets business reviews.
     *
     * @param id the id
     * @return the business reviews
     */
    @GetMapping("/{id}/reviews")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Review>> getBusinessReviews(@PathVariable String id) {
        return new ResponseEntity<>(reviewService.getReviewByBusiness(id), HttpStatus.OK);
    }


    /**
     * Gets business appointments.
     *
     * @param id the id
     * @return the business appointments
     */
    @GetMapping("/{id}/appointments")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<AppointmentDTO>> getBusinessAppointments(@PathVariable String id) {
        return new ResponseEntity<>(appointmentService.getAppointmentByBusiness(id), HttpStatus.OK);
    }


}
