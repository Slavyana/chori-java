package com.example.chori.controller;

import com.example.chori.dto.AppointmentDTO;
import com.example.chori.dto.EmployeeDTO;
import com.example.chori.dto.ServiceDTO;
import com.example.chori.model.DateSlots;
import com.example.chori.service.AppointmentService;
import com.example.chori.service.EmployeeService;
import com.example.chori.service.ServiceService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Employee controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final AppointmentService appointmentService;
    private final ServiceService serviceService;

    /**
     * Instantiates a new Employee controller.
     *
     * @param employeeService    the employee service
     * @param appointmentService the appointment service
     * @param serviceService     the service service
     */
    EmployeeController(EmployeeService employeeService, AppointmentService appointmentService, ServiceService serviceService) {
        this.employeeService = employeeService;
        this.appointmentService = appointmentService;
        this.serviceService = serviceService;
    }

    /**
     * Gets all employees.
     *
     * @return the all employees
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<EmployeeDTO>> getAllEmployees() {
        return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
    }

    /**
     * Create employee response entity.
     *
     * @param employeeDTO the employee dto
     * @return the response entity
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return new ResponseEntity<>(employeeService.createEmployee(employeeDTO), HttpStatus.CREATED);
    }

    /**
     * Gets employee by id.
     *
     * @param id the id
     * @return the employee by id
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable String id) {
        return new ResponseEntity<>(employeeService.getEmployeeById(id), HttpStatus.OK);
    }

    /**
     * Update employee response entity.
     *
     * @param employeeDTO the employee dto
     * @param id          the id
     * @return the response entity
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<EmployeeDTO> updateEmployee(@RequestBody EmployeeDTO employeeDTO, @PathVariable String id) {
        return new ResponseEntity<>(employeeService.updateEmployee(employeeDTO, id), HttpStatus.OK);
    }

    /**
     * Delete employee response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<EmployeeDTO> deleteEmployee(@PathVariable String id) {
        return new ResponseEntity<>(employeeService.deleteEmployeeById(id), HttpStatus.OK);
    }

    /**
     * Gets employee appointments.
     *
     * @param id the id
     * @return the employee appointments
     */
    @GetMapping("/{id}/appointments")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<AppointmentDTO>> getEmployeeAppointments(@PathVariable String id) {
        return new ResponseEntity<>(appointmentService.getAppointmentByEmployee(id), HttpStatus.OK);
    }

    /**
     * Gets employee services.
     *
     * @param id the id
     * @return the employee services
     */
    @GetMapping("/{id}/services")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ServiceDTO>> getEmployeeServices(@PathVariable String id) {

        List<ServiceDTO> services = new ArrayList<>();
        EmployeeDTO employee = employeeService.getEmployeeById(id);

        if (employee.getServices() != null) {
            for (String serviceId : employee.getServices()) {
                services.add(serviceService.getServiceById(serviceId));
            }
        }

        return new ResponseEntity<>(services, HttpStatus.OK);
    }


    /**
     * Gets employee slots.
     *
     * @param id   the id
     * @param date the date
     * @return the employee slots
     */
    @GetMapping("/{id}/slots")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<DateSlots>> getEmployeeSlots(@PathVariable String id, @RequestParam(required = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        return new ResponseEntity<>(employeeService.getSlotsByEmployee(id, date), HttpStatus.OK);
    }
}