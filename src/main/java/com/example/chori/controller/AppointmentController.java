package com.example.chori.controller;

import com.example.chori.dto.AppointmentDTO;
import com.example.chori.service.AppointmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The type Appointment controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/appointments")
public class AppointmentController {

    private final AppointmentService appointmentService;


    /**
     * Instantiates a new Appointment controller.
     *
     * @param appointmentService the appointment service
     */
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    /**
     * Gets all appointments.
     *
     * @return the all appointments
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<AppointmentDTO>> getAllAppointments() {
        return new ResponseEntity<>(appointmentService.getAllAppointments(), HttpStatus.OK);
    }

    /**
     * Create appointment response entity.
     *
     * @param appointmentDTO the appointment dto
     * @return the response entity
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AppointmentDTO> createAppointment(@RequestBody @Valid AppointmentDTO appointmentDTO) {
        return new ResponseEntity<>(appointmentService.createAppointment(appointmentDTO), HttpStatus.CREATED);
    }

    /**
     * Gets appointment by id.
     *
     * @param id the id
     * @return the appointment by id
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AppointmentDTO> getAppointmentById(@PathVariable String id) {
        return new ResponseEntity<>(appointmentService.getAppointmentById(id), HttpStatus.OK);
    }

    /**
     * Update appointment response entity.
     *
     * @param appointmentDTO the appointment dto
     * @param id             the id
     * @return the response entity
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AppointmentDTO> updateAppointment(@RequestBody AppointmentDTO appointmentDTO, @PathVariable String id) {
        return new ResponseEntity<>(appointmentService.updateAppointment(appointmentDTO, id), HttpStatus.OK);
    }

    /**
     * Delete appointment response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AppointmentDTO> deleteAppointment(@PathVariable String id) {
        return new ResponseEntity<>(appointmentService.deleteAppointmentById(id), HttpStatus.OK);
    }

}
