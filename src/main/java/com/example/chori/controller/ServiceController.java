package com.example.chori.controller;

import com.example.chori.dto.ServiceDTO;
import com.example.chori.service.ServiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * The type Service controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/services")
public class ServiceController {

    private final ServiceService serviceService;

    /**
     * Instantiates a new Service controller.
     *
     * @param serviceService the service service
     */
    public ServiceController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    /**
     * Gets all services.
     *
     * @return the all services
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ServiceDTO>> getAllServices() {
        return new ResponseEntity<>(serviceService.getAllServices(), HttpStatus.OK);
    }

    /**
     * Create service response entity.
     *
     * @param serviceDTO the service dto
     * @return the response entity
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceDTO> createService(@RequestBody ServiceDTO serviceDTO) {
        return new ResponseEntity<>(serviceService.createService(serviceDTO), HttpStatus.CREATED);
    }

    /**
     * Gets service by id.
     *
     * @param id the id
     * @return the service by id
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ServiceDTO> getServiceById(@PathVariable String id) {
        return new ResponseEntity<>(serviceService.getServiceById(id), HttpStatus.OK);
    }

    /**
     * Update service response entity.
     *
     * @param serviceDTO the service dto
     * @param id         the id
     * @return the response entity
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ServiceDTO> updateService(@RequestBody ServiceDTO serviceDTO, @PathVariable String id) {
        return new ResponseEntity<>(serviceService.updateService(serviceDTO, id), HttpStatus.OK);
    }

    /**
     * Delete service response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ServiceDTO> deleteService(@PathVariable String id) {
        return new ResponseEntity<>(serviceService.deleteServiceById(id), HttpStatus.OK);
    }

}
