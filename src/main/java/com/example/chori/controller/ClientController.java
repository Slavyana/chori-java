package com.example.chori.controller;

import com.example.chori.dto.AppointmentDTO;
import com.example.chori.dto.ClientDTO;
import com.example.chori.model.Post;
import com.example.chori.service.AppointmentService;
import com.example.chori.service.ClientService;
import com.example.chori.service.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type Client controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/clients")
public class ClientController {

    private final ClientService clientService;
    private final PostService postService;
    private final AppointmentService appointmentService;

    /**
     * Instantiates a new Client controller.
     *
     * @param clientService      the client service
     * @param postService        the post service
     * @param appointmentService the appointment service
     */
    ClientController(ClientService clientService, PostService postService, AppointmentService appointmentService) {
        this.clientService = clientService;
        this.postService = postService;
        this.appointmentService = appointmentService;
    }


    /**
     * Gets all clients.
     *
     * @param search the search
     * @return the all clients
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ClientDTO>> getAllClients(@RequestParam(required = false) String search) {
        if (search == null) {
            return new ResponseEntity<>(clientService.getAllClients(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(clientService.getAllClientsBySearch(search), HttpStatus.OK);
        }
    }

    /**
     * Create client response entity.
     *
     * @param clientDTO the client dto
     * @return the response entity
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ClientDTO> createClient(@RequestBody ClientDTO clientDTO) {
        return new ResponseEntity<>(clientService.createClient(clientDTO), HttpStatus.CREATED);
    }

    /**
     * Gets client by id.
     *
     * @param id the id
     * @return the client by id
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ClientDTO> getClientById(@PathVariable String id) {
        return new ResponseEntity<>(clientService.getClientById(id), HttpStatus.OK);
    }

    /**
     * Update client response entity.
     *
     * @param clientDTO the client dto
     * @param id        the id
     * @return the response entity
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ClientDTO> updateClient(@RequestBody ClientDTO clientDTO, @PathVariable String id) {
        return new ResponseEntity<>(clientService.updateClient(clientDTO, id), HttpStatus.OK);
    }

    /**
     * Delete client response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ClientDTO> deleteClient(@PathVariable String id) {
        return new ResponseEntity<>(clientService.deleteClientById(id), HttpStatus.OK);
    }

    /**
     * Gets services.
     *
     * @param id the id
     * @return the services
     */
    @GetMapping("/{id}/posts")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Post>> getServices(@PathVariable String id) {
        return new ResponseEntity<>(postService.getPostsByAuthor(id), HttpStatus.OK);
    }

    /**
     * Gets business appointments.
     *
     * @param id the id
     * @return the business appointments
     */
    @GetMapping("/{id}/appointments")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<AppointmentDTO>> getBusinessAppointments(@PathVariable String id) {
        return new ResponseEntity<>(appointmentService.getAppointmentByClient(id), HttpStatus.OK);
    }
}
