package com.example.chori.controller;

import com.example.chori.dto.*;
import com.example.chori.dto.mapper.ClientMapper;
import com.example.chori.dto.mapper.EmployeeMapper;
import com.example.chori.jwt.JwtUtils;
import com.example.chori.model.ERole;
import com.example.chori.model.Role;
import com.example.chori.model.User;
import com.example.chori.repository.RoleRepository;
import com.example.chori.repository.UserRepository;
import com.example.chori.service.BusinessService;
import com.example.chori.service.ClientService;
import com.example.chori.service.EmployeeService;
import com.example.chori.service.UserDetailsImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The type Auth controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final BusinessService businessService;

    private final EmployeeService employeeService;

    private final ClientService clientService;

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    private final JwtUtils jwtUtils;

    /**
     * Instantiates a new Auth controller.
     *
     * @param businessService       the business service
     * @param employeeService       the employee service
     * @param clientService         the client service
     * @param authenticationManager the authentication manager
     * @param userRepository        the user repository
     * @param roleRepository        the role repository
     * @param encoder               the encoder
     * @param jwtUtils              the jwt utils
     */
    public AuthController(BusinessService businessService, EmployeeService employeeService, ClientService clientService, AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, JwtUtils jwtUtils) {
        this.businessService = businessService;
        this.employeeService = employeeService;
        this.clientService = clientService;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    /**
     * Authenticate user response entity.
     *
     * @param loginRequest the login request
     * @return the response entity
     */
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        if (userDetails.getEmployee() != null) {
            EmployeeDTO employeeDto = EmployeeMapper.toEmployeeDTO(userDetails.getEmployee());

            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    employeeDto,
                    roles));
        } else {
            ClientDTO clientDTO = ClientMapper.toClientDTO(userDetails.getClient());

            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    clientDTO,
                    roles));
        }


    }

    /**
     * Register user response entity.
     *
     * @param signUpRequest the sign up request
     * @return the response entity
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRoles();
        Set<Role> roles = new HashSet<>();


        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);

            ClientDTO clientDTO = signUpRequest.getClient();
            clientDTO = clientService.createClient(clientDTO);

            user.setClient(ClientMapper.toClient(clientDTO));
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "employee":
                        Role employeeRole = roleRepository.findByName(ERole.ROLE_EMPLOYEE)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(employeeRole);

                        BusinessDTO businessDTO = businessService.getBusinessById(signUpRequest.getBusiness().getId());

                        EmployeeDTO employeeDTO = signUpRequest.getEmployee();
                        employeeDTO.setBusiness(businessDTO);
                        employeeDTO = employeeService.createEmployee(employeeDTO);

                        user.setEmployee(EmployeeMapper.toEmployee(employeeDTO));

                        break;

                    case "manager":
                        Role managerRole = roleRepository.findByName(ERole.ROLE_MANAGER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(managerRole);


                        businessDTO = businessService.createBusiness(signUpRequest.getBusiness());

                        employeeDTO = signUpRequest.getEmployee();
                        employeeDTO.setBusiness(businessDTO);
                        employeeDTO = employeeService.createEmployee(employeeDTO);

                        user.setEmployee(EmployeeMapper.toEmployee(employeeDTO));

                        break;

                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);

                        ClientDTO clientDTO = signUpRequest.getClient();
                        clientDTO = clientService.createClient(clientDTO);

                        user.setClient(ClientMapper.toClient(clientDTO));
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}