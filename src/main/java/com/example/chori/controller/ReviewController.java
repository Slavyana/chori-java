package com.example.chori.controller;

import com.example.chori.model.Review;
import com.example.chori.service.ReviewService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type Review controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/reviews")
public class ReviewController {

    private final ReviewService reviewService;

    /**
     * Instantiates a new Review controller.
     *
     * @param reviewService the review service
     */
    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    /**
     * Gets all reviews.
     *
     * @return the all reviews
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Review>> getAllReviews() {
        return new ResponseEntity<>(reviewService.getAllReviews(), HttpStatus.OK);
    }

    /**
     * Create review response entity.
     *
     * @param review the review
     * @return the response entity
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Review> createReview(@RequestBody Review review) {
        return new ResponseEntity<>(reviewService.createReview(review), HttpStatus.CREATED);
    }

    /**
     * Gets review by id.
     *
     * @param id the id
     * @return the review by id
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Review> getReviewById(@PathVariable String id) {
        return new ResponseEntity<>(reviewService.getReviewById(id), HttpStatus.OK);
    }

    /**
     * Update review response entity.
     *
     * @param review the review
     * @param id     the id
     * @return the response entity
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Review> updateReview(@RequestBody Review review, @PathVariable String id) {
        return new ResponseEntity<>(reviewService.updateReview(review, id), HttpStatus.OK);
    }

    /**
     * Delete review response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Review> deleteReview(@PathVariable String id) {
        return new ResponseEntity<>(reviewService.deleteReviewById(id), HttpStatus.OK);
    }
}
