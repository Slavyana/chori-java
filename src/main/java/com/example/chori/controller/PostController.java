package com.example.chori.controller;

import com.example.chori.model.Comment;
import com.example.chori.model.Post;
import com.example.chori.model.User;
import com.example.chori.service.CommentService;
import com.example.chori.service.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type Post controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/posts")
public class PostController {

    private final PostService postService;
    private final CommentService commentService;

    /**
     * Instantiates a new Post controller.
     *
     * @param postService    the post service
     * @param commentService the comment service
     */
    PostController(PostService postService, CommentService commentService) {
        this.postService = postService;
        this.commentService = commentService;
    }

    /**
     * Gets all posts.
     *
     * @return the all posts
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Post>> getAllPosts() {
        return new ResponseEntity<>(postService.getAllPosts(), HttpStatus.OK);
    }

    /**
     * Create post response entity.
     *
     * @param post the post
     * @return the response entity
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Post> createPost(@RequestBody Post post) {
        return new ResponseEntity<>(postService.createPost(post), HttpStatus.CREATED);
    }

    /**
     * Gets post by id.
     *
     * @param id the id
     * @return the post by id
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Post> getPostById(@PathVariable String id) {
        return new ResponseEntity<>(postService.getPostById(id), HttpStatus.OK);
    }

    /**
     * Update post response entity.
     *
     * @param post the post
     * @param id   the id
     * @return the response entity
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Post> updatePost(@RequestBody Post post, @PathVariable String id) {
        return new ResponseEntity<>(postService.updatePost(post, id), HttpStatus.OK);
    }

    /**
     * Delete post response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Post> deletePost(@PathVariable String id) {
        return new ResponseEntity<>(postService.deletePostById(id), HttpStatus.OK);
    }

    /**
     * Add comment response entity.
     *
     * @param postId  the post id
     * @param comment the comment
     * @return the response entity
     */
    @PostMapping("/{postId}/comment")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Post> addComment(@PathVariable String postId, @RequestBody Comment comment) {
        Post post = postService.getPostById(postId);
        post.addComment(commentService.createComment(comment));
        return new ResponseEntity<>(postService.updatePost(post, postId), HttpStatus.CREATED);
    }

    /**
     * Edit comment response entity.
     *
     * @param postId    the post id
     * @param commentId the comment id
     * @param comment   the comment
     * @return the response entity
     */
    @PutMapping("/{postId}/comment/{commentId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Post> editComment(@PathVariable String postId, @PathVariable String commentId, @RequestBody Comment comment) {
        Post post = postService.getPostById(postId);
        post.editComment(commentService.updateComment(comment, commentId));
        return new ResponseEntity<>(postService.updatePost(post, postId), HttpStatus.OK);
    }

    /**
     * Delete comment response entity.
     *
     * @param postId    the post id
     * @param commentId the comment id
     * @return the response entity
     */
    @DeleteMapping("/{postId}/comment/{commentId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Post> deleteComment(@PathVariable String postId, @PathVariable String commentId) {
        Post post = postService.getPostById(postId);
        post.deleteComment(commentId);
        commentService.deleteCommentById(commentId);
        return new ResponseEntity<>(postService.updatePost(post, postId), HttpStatus.OK);
    }

    /**
     * Like post response entity.
     *
     * @param postId the post id
     * @param user   the user
     * @return the response entity
     */
    @PostMapping("/{postId}/like")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Post> likePost(@PathVariable String postId, @RequestBody User user) {
        Post post = postService.getPostById(postId);
        post.like(user);
        return new ResponseEntity<>(postService.updatePost(post, postId), HttpStatus.CREATED);
    }

    /**
     * Unlike post response entity.
     *
     * @param postId the post id
     * @param userId the user id
     * @return the response entity
     */
    @DeleteMapping("/{postId}/unlike/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Post> unlikePost(@PathVariable String postId, @PathVariable String userId) {
        Post post = postService.getPostById(postId);
        post.unlike(userId);
        return new ResponseEntity<>(postService.updatePost(post, postId), HttpStatus.OK);
    }

    /**
     * Like comment response entity.
     *
     * @param postId    the post id
     * @param commentId the comment id
     * @param user      the user
     * @return the response entity
     */
    @PostMapping("/{postId}/{commentId}/like")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Post> likeComment(@PathVariable String postId, @PathVariable String commentId, @RequestBody User user) {
        Post post = postService.getPostById(postId);
        Comment comment = commentService.getCommentById(commentId);
        comment.like(user);
        post.editComment(comment);
        commentService.updateComment(comment, commentId);
        return new ResponseEntity<>(postService.updatePost(post, postId), HttpStatus.CREATED);
    }

    /**
     * Unlike comment response entity.
     *
     * @param postId    the post id
     * @param commentId the comment id
     * @param userId    the user id
     * @return the response entity
     */
    @DeleteMapping("/{postId}/{commentId}/unlike/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Post> unlikeComment(@PathVariable String postId, @PathVariable String commentId, @PathVariable String userId) {
        Post post = postService.getPostById(postId);
        Comment comment = commentService.getCommentById(commentId);
        comment.unlike(userId);
        post.editComment(comment);
        commentService.updateComment(comment, commentId);
        return new ResponseEntity<>(postService.updatePost(post, postId), HttpStatus.OK);
    }


}
