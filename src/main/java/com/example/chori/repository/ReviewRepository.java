package com.example.chori.repository;

import com.example.chori.model.Review;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * The interface Review repository.
 */
@RepositoryRestResource(collectionResourceRel = "reviews", path = "reviews")
public interface ReviewRepository extends MongoRepository<Review, String> {

    /**
     * Find by business id list.
     *
     * @param businessId the business id
     * @return the list
     */
    List<Review> findByBusinessId(String businessId);
}
