package com.example.chori.repository;

import com.example.chori.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * The interface Employee repository.
 */
@RepositoryRestResource(collectionResourceRel = "employees", path = "employees")
public interface EmployeeRepository extends MongoRepository<Employee, String> {
    /**
     * Find by last name list.
     *
     * @param name the name
     * @return the list
     */
    List<Employee> findByLastName(@Param("name") String name);

    /**
     * Find by business id list.
     *
     * @param businessId the business id
     * @return the list
     */
    List<Employee> findByBusinessId(String businessId);
}