package com.example.chori.repository;

import com.example.chori.model.Service;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * The interface Service repository.
 */
@RepositoryRestResource(collectionResourceRel = "services", path = "services")
public interface ServiceRepository extends MongoRepository<Service, String> {

    /**
     * Find by business list.
     *
     * @param businessId the business id
     * @return the list
     */
    @Query("{ 'business' : ?0 }")
    List<Service> findByBusiness(String businessId);

    /**
     * Find by author list.
     *
     * @param authorId the author id
     * @return the list
     */
    @Query("{ 'author' : ?0 }")
    List<Service> findByAuthor(String authorId);
}