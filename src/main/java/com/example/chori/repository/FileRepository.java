package com.example.chori.repository;

import com.example.chori.model.File;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


/**
 * The interface File repository.
 */
@RepositoryRestResource(collectionResourceRel = "files", path = "files")
public interface FileRepository extends MongoRepository<File, String> {
}
