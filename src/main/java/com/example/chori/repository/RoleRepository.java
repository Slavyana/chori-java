package com.example.chori.repository;

import com.example.chori.model.ERole;
import com.example.chori.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * The interface Role repository.
 */
public interface RoleRepository extends MongoRepository<Role, String> {
    /**
     * Find by name optional.
     *
     * @param name the name
     * @return the optional
     */
    Optional<Role> findByName(ERole name);
}