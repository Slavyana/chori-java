package com.example.chori.repository;

import com.example.chori.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * The interface Product repository.
 */
@RepositoryRestResource(collectionResourceRel = "products", path = "products")
public interface ProductRepository extends MongoRepository<Product, String> {

    /**
     * Find by business id list.
     *
     * @param businessId the business id
     * @return the list
     */
    List<Product> findByBusinessId(String businessId);

}


