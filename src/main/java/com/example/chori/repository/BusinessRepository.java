package com.example.chori.repository;

import com.example.chori.model.Business;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


/**
 * The interface Business repository.
 */
@RepositoryRestResource(collectionResourceRel = "businesses", path = "businesses")
public interface BusinessRepository extends MongoRepository<Business, String> {

    /**
     * Find by username starting with ignore case list.
     *
     * @param name the name
     * @return the list
     */
    List<Business> findByUsernameStartingWithIgnoreCase(String name);

    /**
     * Find by name starting with ignore case list.
     *
     * @param name the name
     * @return the list
     */
    List<Business> findByNameStartingWithIgnoreCase(String name);

    /**
     * Find by country ignore case list.
     *
     * @param country the country
     * @return the list
     */
    List<Business> findByCountryIgnoreCase(String country);

    /**
     * Find by country and region all ignore case list.
     *
     * @param country the country
     * @param region  the region
     * @return the list
     */
    List<Business> findByCountryAndRegionAllIgnoreCase(String country, String region);


}
