package com.example.chori.repository;

import com.example.chori.model.Post;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * The interface Post repository.
 */
@RepositoryRestResource(collectionResourceRel = "posts", path = "posts")
public interface PostRepository extends MongoRepository<Post, String> {

    /**
     * Find by business id list.
     *
     * @param businessId the business id
     * @return the list
     */
    List<Post> findByBusinessId(String businessId);

    /**
     * Find by author id list.
     *
     * @param id the id
     * @return the list
     */
    List<Post> findByAuthorId(String id);
}


