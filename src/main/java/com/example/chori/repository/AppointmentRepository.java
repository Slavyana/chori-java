package com.example.chori.repository;

import com.example.chori.model.Appointment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * The interface Appointment repository.
 */
@RepositoryRestResource(collectionResourceRel = "appointments", path = "appointments")
public interface AppointmentRepository extends MongoRepository<Appointment, String> {
    /**
     * Find by business id list.
     *
     * @param businessId the business id
     * @return the list
     */
    List<Appointment> findByBusinessId(String businessId);

    /**
     * Find by employee id list.
     *
     * @param employeeId the employee id
     * @return the list
     */
    List<Appointment> findByEmployeeId(String employeeId);

    /**
     * Find by client id list.
     *
     * @param clientId the client id
     * @return the list
     */
    List<Appointment> findByClientId(String clientId);
}
