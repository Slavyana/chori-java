package com.example.chori.repository;


import com.example.chori.model.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * The interface Client repository.
 */
@RepositoryRestResource(collectionResourceRel = "clients", path = "clients")
public interface ClientRepository extends MongoRepository<Client, String> {

    /**
     * Find by username starting with ignore case list.
     *
     * @param name the name
     * @return the list
     */
    List<Client> findByUsernameStartingWithIgnoreCase(String name);

    /**
     * Find by first name starting with or last name starting with all ignore case list.
     *
     * @param firstName the first name
     * @param lastName  the last name
     * @return the list
     */
    List<Client> findByFirstNameStartingWithOrLastNameStartingWithAllIgnoreCase(String firstName, String lastName);
}
