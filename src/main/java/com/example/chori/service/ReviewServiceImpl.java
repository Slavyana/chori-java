package com.example.chori.service;

import com.example.chori.model.Review;
import com.example.chori.repository.ReviewRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Review service.
 */
@Service
public class ReviewServiceImpl implements ReviewService {
    private final ReviewRepository reviewRepository;

    /**
     * Instantiates a new Review service.
     *
     * @param reviewRepository the review repository
     */
    public ReviewServiceImpl(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public List<Review> getAllReviews() {
        return new ArrayList<>(reviewRepository
                .findAll());
    }

    @Override
    public Review getReviewById(String id) {
        if (reviewRepository.findById(id).isPresent())
            return reviewRepository.findById(id).get();
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public Review createReview(Review review) {
        return reviewRepository.save(review);
    }

    @Override
    public Review updateReview(Review review, String id) {
        if (reviewRepository.findById(id).isPresent()) {
            review.setId(id);
            return reviewRepository.save(review);
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public Review deleteReviewById(String id) {
        if (reviewRepository.findById(id).isPresent()) {
            Review review = reviewRepository.findById(id).get();
            reviewRepository.delete(review);
            return review;
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public List<Review> getReviewByBusiness(String id) {
        var result = reviewRepository.findByBusinessId(id);

        Collections.reverse(result);
        return result;
    }
}
