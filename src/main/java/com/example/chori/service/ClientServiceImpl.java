package com.example.chori.service;

import com.example.chori.dto.ClientDTO;
import com.example.chori.dto.mapper.ClientMapper;
import com.example.chori.model.Client;
import com.example.chori.repository.ClientRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Client service.
 */
@Service
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;

    /**
     * Instantiates a new Client service.
     *
     * @param clientRepository the client repository
     */
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public List<ClientDTO> getAllClients() {
        return clientRepository
                .findAll()
                .stream()
                .map(ClientMapper::toClientDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ClientDTO getClientById(String id) {

        if (clientRepository.findById(id).isPresent())
            return ClientMapper.toClientDTO(clientRepository.findById(id).get());
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public ClientDTO createClient(ClientDTO clientDTO) {
        Client client = ClientMapper.toClient(clientDTO);
        return ClientMapper.toClientDTO(clientRepository.save(client));
    }

    @Override
    public ClientDTO updateClient(ClientDTO clientDTO, String id) {
        if (clientRepository.findById(id).isPresent()) {
            clientDTO.setId(id);
            Client existingClient = ClientMapper.toClient(clientDTO);
            return ClientMapper.toClientDTO(clientRepository.save(existingClient));
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public ClientDTO deleteClientById(String id) {
        if (clientRepository.findById(id).isPresent()) {
            Client client = clientRepository.findById(id).get();
            clientRepository.delete(client);
            return ClientMapper.toClientDTO(client);
        } else
            throw new MongoException("Record not found");
    }

    /**
     * Distinct by key predicate.
     *
     * @param <T>          the type parameter
     * @param keyExtractor the key extractor
     * @return the predicate
     */
    public static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }


    @Override
    public List<ClientDTO> getAllClientsBySearch(String search) {
        List<ClientDTO> listByUsername = clientRepository
                .findByUsernameStartingWithIgnoreCase(search)
                .stream()
                .map(ClientMapper::toClientDTO)
                .collect(Collectors.toList());

        String[] names = search.split(" ");

        List<ClientDTO> listByName = clientRepository
                .findByFirstNameStartingWithOrLastNameStartingWithAllIgnoreCase(names[0], names[names.length - 1])
                .stream()
                .map(ClientMapper::toClientDTO)
                .collect(Collectors.toList());


        List<ClientDTO> combine = Stream.concat(listByUsername.stream(), listByName.stream())
                .collect(Collectors.toList());


        return combine.stream()
                .filter(distinctByKey(ClientDTO::getId))
                .collect(Collectors.toList());
    }

}
