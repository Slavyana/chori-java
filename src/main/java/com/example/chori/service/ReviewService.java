package com.example.chori.service;

import com.example.chori.model.Review;

import java.util.List;

/**
 * The interface Review service.
 */
public interface ReviewService {
    /**
     * Gets all reviews.
     *
     * @return the all reviews
     */
    List<Review> getAllReviews();

    /**
     * Gets review by id.
     *
     * @param id the id
     * @return the review by id
     */
    Review getReviewById(String id);

    /**
     * Create review review.
     *
     * @param review the review
     * @return the review
     */
    Review createReview(Review review);

    /**
     * Update review review.
     *
     * @param review the review
     * @param id     the id
     * @return the review
     */
    Review updateReview(Review review, String id);

    /**
     * Delete review by id review.
     *
     * @param id the id
     * @return the review
     */
    Review deleteReviewById(String id);

    /**
     * Gets review by business.
     *
     * @param id the id
     * @return the review by business
     */
    List<Review> getReviewByBusiness(String id);
}
