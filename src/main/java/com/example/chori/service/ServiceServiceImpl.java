package com.example.chori.service;

import com.example.chori.dto.ServiceDTO;
import com.example.chori.dto.mapper.ServiceMapper;
import com.example.chori.model.Service;
import com.example.chori.repository.ServiceRepository;
import com.mongodb.MongoException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Service service.
 */
@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository serviceRepository;

    /**
     * Instantiates a new Service service.
     *
     * @param serviceRepository the service repository
     */
    public ServiceServiceImpl(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @Override
    public List<ServiceDTO> getAllServices() {
        return serviceRepository
                .findAll()
                .stream()
                .map(ServiceMapper::toServiceDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ServiceDTO getServiceById(String id) {
        if (serviceRepository.findById(id).isPresent())
            return ServiceMapper.toServiceDTO(serviceRepository.findById(id).get());
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public ServiceDTO createService(ServiceDTO serviceDTO) {
        Service service = ServiceMapper.toService(serviceDTO);
        return ServiceMapper.toServiceDTO(serviceRepository.save(service));
    }

    @Override
    public ServiceDTO updateService(ServiceDTO serviceDTO, String id) {
        if (serviceRepository.findById(id).isPresent()) {
            serviceDTO.setId(id);
            Service existingService = ServiceMapper.toService(serviceDTO);
            return ServiceMapper.toServiceDTO(serviceRepository.save(existingService));
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public ServiceDTO deleteServiceById(String id) {
        if (serviceRepository.findById(id).isPresent()) {
            Service service = serviceRepository.findById(id).get();
            serviceRepository.delete(service);
            return ServiceMapper.toServiceDTO(service);
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public List<ServiceDTO> getServicesByBusiness(String businessId) {


        List<ServiceDTO> serviceList = serviceRepository.findByBusiness(businessId)
                .stream()
                .map(ServiceMapper::toServiceDTO)
                .collect(Collectors.toList());

        return serviceList;
    }
}
