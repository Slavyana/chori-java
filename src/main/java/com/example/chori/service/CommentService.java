package com.example.chori.service;

import com.example.chori.model.Comment;

/**
 * The interface Comment service.
 */
public interface CommentService {
    /**
     * Gets comment by id.
     *
     * @param id the id
     * @return the comment by id
     */
    Comment getCommentById(String id);

    /**
     * Create comment comment.
     *
     * @param comment the comment
     * @return the comment
     */
    Comment createComment(Comment comment);

    /**
     * Update comment comment.
     *
     * @param comment the comment
     * @param id      the id
     * @return the comment
     */
    Comment updateComment(Comment comment, String id);

    /**
     * Delete comment by id comment.
     *
     * @param id the id
     * @return the comment
     */
    Comment deleteCommentById(String id);
}
