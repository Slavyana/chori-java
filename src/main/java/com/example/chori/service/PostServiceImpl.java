package com.example.chori.service;

import com.example.chori.model.Post;
import com.example.chori.repository.PostRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Post service.
 */
@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;

    /**
     * Instantiates a new Post service.
     *
     * @param postRepository the post repository
     */
    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<Post> getAllPosts() {

        var result = new ArrayList<>(postRepository
                .findAll());
        Collections.reverse(result);

        return result;
    }

    @Override
    public Post getPostById(String id) {
        if (postRepository.findById(id).isPresent())
            return postRepository.findById(id).get();
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public Post createPost(Post post) {
        return postRepository.save(post);
    }

    @Override
    public Post updatePost(Post post, String id) {
        if (postRepository.findById(id).isPresent()) {
            post.setId(id);
            return postRepository.save(post);
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public Post deletePostById(String id) {
        if (postRepository.findById(id).isPresent()) {
            Post post = postRepository.findById(id).get();
            postRepository.delete(post);
            return post;
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public List<Post> getPostsByBusiness(String id) {
        return postRepository.findByBusinessId(id);
    }

    @Override
    public List<Post> getPostsByAuthor(String id) {
        return postRepository.findByAuthorId(id);
    }

}
