package com.example.chori.service;

import com.example.chori.dto.BusinessDTO;

import java.util.List;

/**
 * The interface Business service.
 */
public interface BusinessService {
    /**
     * Gets all businesses.
     *
     * @return the all businesses
     */
    List<BusinessDTO> getAllBusinesses();

    /**
     * Gets all businesses by search.
     *
     * @param search the search
     * @return the all businesses by search
     */
    List<BusinessDTO> getAllBusinessesBySearch(String search);

    /**
     * Gets all businesses by country.
     *
     * @param country the country
     * @return the all businesses by country
     */
    List<BusinessDTO> getAllBusinessesByCountry(String country);

    /**
     * Gets all businesses by country and region.
     *
     * @param country the country
     * @param region  the region
     * @return the all businesses by country and region
     */
    List<BusinessDTO> getAllBusinessesByCountryAndRegion(String country, String region);

    /**
     * Gets business by id.
     *
     * @param id the id
     * @return the business by id
     */
    BusinessDTO getBusinessById(String id);

    /**
     * Create business business dto.
     *
     * @param businessDTO the business dto
     * @return the business dto
     */
    BusinessDTO createBusiness(BusinessDTO businessDTO);

    /**
     * Update business business dto.
     *
     * @param businessDTO the business dto
     * @param id          the id
     * @return the business dto
     */
    BusinessDTO updateBusiness(BusinessDTO businessDTO, String id);

    /**
     * Delete business by id business dto.
     *
     * @param id the id
     * @return the business dto
     */
    BusinessDTO deleteBusinessById(String id);

}
