package com.example.chori.service;

import com.example.chori.model.Post;

import java.util.List;

/**
 * The interface Post service.
 */
public interface PostService {
    /**
     * Gets all posts.
     *
     * @return the all posts
     */
    List<Post> getAllPosts();

    /**
     * Gets post by id.
     *
     * @param id the id
     * @return the post by id
     */
    Post getPostById(String id);

    /**
     * Create post post.
     *
     * @param postDTO the post dto
     * @return the post
     */
    Post createPost(Post postDTO);

    /**
     * Update post post.
     *
     * @param postDTO the post dto
     * @param id      the id
     * @return the post
     */
    Post updatePost(Post postDTO, String id);

    /**
     * Delete post by id post.
     *
     * @param id the id
     * @return the post
     */
    Post deletePostById(String id);

    /**
     * Gets posts by business.
     *
     * @param id the id
     * @return the posts by business
     */
    List<Post> getPostsByBusiness(String id);

    /**
     * Gets posts by author.
     *
     * @param id the id
     * @return the posts by author
     */
    List<Post> getPostsByAuthor(String id);
}
