package com.example.chori.service;

import com.example.chori.dto.ClientDTO;

import java.util.List;

/**
 * The interface Client service.
 */
public interface ClientService {
    /**
     * Gets all clients.
     *
     * @return the all clients
     */
    List<ClientDTO> getAllClients();

    /**
     * Gets client by id.
     *
     * @param id the id
     * @return the client by id
     */
    ClientDTO getClientById(String id);

    /**
     * Create client client dto.
     *
     * @param clientDto the client dto
     * @return the client dto
     */
    ClientDTO createClient(ClientDTO clientDto);

    /**
     * Update client client dto.
     *
     * @param clientDto the client dto
     * @param id        the id
     * @return the client dto
     */
    ClientDTO updateClient(ClientDTO clientDto, String id);

    /**
     * Delete client by id client dto.
     *
     * @param id the id
     * @return the client dto
     */
    ClientDTO deleteClientById(String id);

    /**
     * Gets all clients by search.
     *
     * @param search the search
     * @return the all clients by search
     */
    List<ClientDTO> getAllClientsBySearch(String search);
}
