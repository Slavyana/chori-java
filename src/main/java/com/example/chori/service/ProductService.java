package com.example.chori.service;

import com.example.chori.dto.ProductDTO;

import java.util.List;

/**
 * The interface Product service.
 */
public interface ProductService {
    /**
     * Gets all products.
     *
     * @return the all products
     */
    List<ProductDTO> getAllProducts();

    /**
     * Gets product by id.
     *
     * @param id the id
     * @return the product by id
     */
    ProductDTO getProductById(String id);

    /**
     * Create product product dto.
     *
     * @param productDTO the product dto
     * @return the product dto
     */
    ProductDTO createProduct(ProductDTO productDTO);

    /**
     * Update product product dto.
     *
     * @param productDTO the product dto
     * @param id         the id
     * @return the product dto
     */
    ProductDTO updateProduct(ProductDTO productDTO, String id);

    /**
     * Delete product by id product dto.
     *
     * @param id the id
     * @return the product dto
     */
    ProductDTO deleteProductById(String id);

    /**
     * Gets products by business.
     *
     * @param id the id
     * @return the products by business
     */
    List<ProductDTO> getProductsByBusiness(String id);
}
