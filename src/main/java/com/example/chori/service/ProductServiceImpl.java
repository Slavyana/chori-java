package com.example.chori.service;

import com.example.chori.dto.ProductDTO;
import com.example.chori.dto.mapper.ProductMapper;
import com.example.chori.model.Product;
import com.example.chori.repository.ProductRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Product service.
 */
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    /**
     * Instantiates a new Product service.
     *
     * @param productRepository the product repository
     */
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductDTO> getAllProducts() {
        return productRepository
                .findAll()
                .stream()
                .map(ProductMapper::toProductDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO getProductById(String id) {

        if (productRepository.findById(id).isPresent())
            return ProductMapper.toProductDTO(productRepository.findById(id).get());
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public ProductDTO createProduct(ProductDTO productDTO) {
        Product product = ProductMapper.toProduct(productDTO);
        return ProductMapper.toProductDTO(productRepository.save(product));
    }

    @Override
    public ProductDTO updateProduct(ProductDTO productDTO, String id) {
        if (productRepository.findById(id).isPresent()) {
            productDTO.setId(id);
            Product existingProduct = ProductMapper.toProduct(productDTO);
            return ProductMapper.toProductDTO(productRepository.save(existingProduct));
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public ProductDTO deleteProductById(String id) {
        if (productRepository.findById(id).isPresent()) {
            Product product = productRepository.findById(id).get();
            productRepository.delete(product);
            return ProductMapper.toProductDTO(product);
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public List<ProductDTO> getProductsByBusiness(String id) {
        return productRepository.findByBusinessId(id)
                .stream()
                .map(ProductMapper::toProductDTO)
                .collect(Collectors.toList());
    }
}
