package com.example.chori.service;

import com.example.chori.model.File;
import com.example.chori.repository.FileRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Stream;

/**
 * The type File storage service.
 */
@Service
public class FileStorageService {

    private final FileRepository fileRepository;

    /**
     * Instantiates a new File storage service.
     *
     * @param fileRepository the file repository
     */
    public FileStorageService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    /**
     * Store file.
     *
     * @param file the file
     * @return the file
     * @throws IOException the io exception
     */
    public File store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        File newFile = new File(fileName, file.getContentType(), file.getBytes());

        return fileRepository.save(newFile);
    }

    /**
     * Gets file.
     *
     * @param id the id
     * @return the file
     */
    public File getFile(String id) {
        return fileRepository.findById(id).get();
    }

    /**
     * Gets all files.
     *
     * @return the all files
     */
    public Stream<File> getAllFiles() {
        return fileRepository.findAll().stream();
    }
}
