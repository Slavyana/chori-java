package com.example.chori.service;

import com.example.chori.dto.AppointmentDTO;
import com.example.chori.dto.mapper.AppointmentMapper;
import com.example.chori.model.Appointment;
import com.example.chori.model.DateSlots;
import com.example.chori.model.Employee;
import com.example.chori.repository.AppointmentRepository;
import com.example.chori.repository.EmployeeRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Appointment service.
 */
@Service
public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final EmployeeRepository employeeRepository;

    /**
     * Instantiates a new Appointment service.
     *
     * @param appointmentRepository the appointment repository
     * @param employeeRepository    the employee repository
     */
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, EmployeeRepository employeeRepository) {
        this.appointmentRepository = appointmentRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<AppointmentDTO> getAllAppointments() {
        return appointmentRepository
                .findAll()
                .stream()
                .map(AppointmentMapper::toAppointmentDTO)
                .collect(Collectors.toList());
    }

    @Override
    public AppointmentDTO getAppointmentById(String id) {
        if (appointmentRepository.findById(id).isPresent())
            return AppointmentMapper.toAppointmentDTO(appointmentRepository.findById(id).get());
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public AppointmentDTO createAppointment(AppointmentDTO appointmentDTO) {
        Appointment appointment = AppointmentMapper.toAppointment(appointmentDTO);

        Optional<Employee> optionalEmployee = employeeRepository.findById(appointment.getEmployee().getId());
        Employee employee = optionalEmployee.orElse(null);

        if (employee != null) {
            List<DateSlots> employeeDateSlotsList = employee.getDateSlotsList();
            SimpleDateFormat dateOnly = new SimpleDateFormat("yyyy-MM-dd");

            Date date = appointmentDTO.getDateSlots().getDate();

            // If the employee slots are not null
            if (employeeDateSlotsList != null) {
                Optional<DateSlots> optionalDateSlots = employeeDateSlotsList.stream().filter(d -> dateOnly.format(d.getDate()).equals(dateOnly.format(date))).findFirst();
               
                DateSlots dateSlots = optionalDateSlots.orElse(null);

                // If there are no date slots with the same date
                if (dateSlots != null) {

                    employeeDateSlotsList.remove(dateSlots);

                    List<Integer> slots = dateSlots.getSlots();

                    List<Integer> newList = Stream.concat(slots.stream(), appointment.getDateSlots().getSlots().stream())
                            .collect(Collectors.toList());

                    dateSlots.setSlots(newList);

                    employeeDateSlotsList.add(dateSlots);

                    employee.setDateSlotsList(employeeDateSlotsList);
                    employeeRepository.save(employee);
                } else {
                    List<DateSlots> newList = Stream.concat(employeeDateSlotsList.stream(), List.of(appointment.getDateSlots()).stream())
                            .collect(Collectors.toList());

                    employee.setDateSlotsList(newList);
                    employeeRepository.save(employee);
                }
            } else {
                employee.setDateSlotsList(List.of(appointment.getDateSlots()));
                employeeRepository.save(employee);
            }

        }

        return AppointmentMapper.toAppointmentDTO(appointmentRepository.save(appointment));
    }

    @Override
    public AppointmentDTO updateAppointment(AppointmentDTO appointmentDTO, String id) {
        if (appointmentRepository.findById(id).isPresent()) {
            appointmentDTO.setId(id);
            Appointment existingAppointment = AppointmentMapper.toAppointment(appointmentDTO);
            return AppointmentMapper.toAppointmentDTO(appointmentRepository.save(existingAppointment));
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public AppointmentDTO deleteAppointmentById(String id) {
        if (appointmentRepository.findById(id).isPresent()) {
            Appointment appointment = appointmentRepository.findById(id).get();


            Optional<Employee> oe = employeeRepository.findById(appointment.getEmployee().getId());
            Employee employee = oe.orElse(null);

            if (employee != null) {
                List<DateSlots> dsl = employee.getDateSlotsList();
                SimpleDateFormat dateOnly = new SimpleDateFormat("yyyy-MM-dd");

                Date date = appointment.getDateSlots().getDate();

                // employee slots not null
                if (dsl != null) {
                    Optional<DateSlots> ods = dsl.stream().filter(d -> dateOnly.format(d.getDate()).equals(dateOnly.format(date))).findFirst();
                    DateSlots ds = ods.orElse(null);

                    // date slot is not null
                    if (ds != null) {
                        dsl.remove(ds);
                        List<Integer> slots = ds.getSlots();
                        slots.removeAll(appointment.getDateSlots().getSlots());
                        ds.setSlots(slots);
                        dsl.add(ds);
                        employee.setDateSlotsList(dsl);
                        employeeRepository.save(employee);
                    }
                }
            }

            appointmentRepository.delete(appointment);
            return AppointmentMapper.toAppointmentDTO(appointment);
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public List<AppointmentDTO> getAppointmentByBusiness(String businessId) {
        List<AppointmentDTO> appointmentList = appointmentRepository.findByBusinessId(businessId)
                .stream()
                .map(AppointmentMapper::toAppointmentDTO)
                .collect(Collectors.toList());

        return appointmentList;
    }

    @Override
    public List<AppointmentDTO> getAppointmentByEmployee(String employeeId) {
        List<AppointmentDTO> appointmentList = appointmentRepository.findByEmployeeId(employeeId)
                .stream()
                .map(AppointmentMapper::toAppointmentDTO)
                .collect(Collectors.toList());

        return appointmentList;
    }

    @Override
    public List<AppointmentDTO> getAppointmentByClient(String clientId) {
        List<AppointmentDTO> appointmentList = appointmentRepository.findByClientId(clientId)
                .stream()
                .map(AppointmentMapper::toAppointmentDTO)
                .collect(Collectors.toList());

        return appointmentList;
    }
}
