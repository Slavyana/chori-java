package com.example.chori.service;

import com.example.chori.dto.AppointmentDTO;

import java.util.List;

/**
 * The interface Appointment service.
 */
public interface AppointmentService {
    /**
     * Gets all appointments.
     *
     * @return the all appointments
     */
    List<AppointmentDTO> getAllAppointments();

    /**
     * Gets appointment by id.
     *
     * @param id the id
     * @return the appointment by id
     */
    AppointmentDTO getAppointmentById(String id);

    /**
     * Create appointment appointment dto.
     *
     * @param appointmentDTO the appointment dto
     * @return the appointment dto
     */
    AppointmentDTO createAppointment(AppointmentDTO appointmentDTO);

    /**
     * Update appointment appointment dto.
     *
     * @param appointmentDTO the appointment dto
     * @param id             the id
     * @return the appointment dto
     */
    AppointmentDTO updateAppointment(AppointmentDTO appointmentDTO, String id);

    /**
     * Delete appointment by id appointment dto.
     *
     * @param id the id
     * @return the appointment dto
     */
    AppointmentDTO deleteAppointmentById(String id);

    /**
     * Gets appointment by business.
     *
     * @param businessId the business id
     * @return the appointment by business
     */
    List<AppointmentDTO> getAppointmentByBusiness(String businessId);

    /**
     * Gets appointment by employee.
     *
     * @param employeeId the employee id
     * @return the appointment by employee
     */
    List<AppointmentDTO> getAppointmentByEmployee(String employeeId);

    /**
     * Gets appointment by client.
     *
     * @param clientId the client id
     * @return the appointment by client
     */
    List<AppointmentDTO> getAppointmentByClient(String clientId);
}
