package com.example.chori.service;

import com.example.chori.dto.BusinessDTO;
import com.example.chori.dto.mapper.BusinessMapper;
import com.example.chori.model.Business;
import com.example.chori.repository.BusinessRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Business service.
 */
@Service
public class BusinessServiceImpl implements BusinessService {
    private final BusinessRepository businessRepository;

    /**
     * Instantiates a new Business service.
     *
     * @param businessRepository the business repository
     */
    public BusinessServiceImpl(BusinessRepository businessRepository) {
        this.businessRepository = businessRepository;
    }

    @Override
    public List<BusinessDTO> getAllBusinesses() {
        return businessRepository
                .findAll()
                .stream()
                .map(BusinessMapper::toBusinessDTO)
                .collect(Collectors.toList());
    }

    /**
     * Distinct by key predicate.
     *
     * @param <T>          the type parameter
     * @param keyExtractor the key extractor
     * @return the predicate
     */
    public static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    @Override
    public List<BusinessDTO> getAllBusinessesBySearch(String search) {
        List<BusinessDTO> listByUsername = businessRepository
                .findByUsernameStartingWithIgnoreCase(search)
                .stream()
                .map(BusinessMapper::toBusinessDTO)
                .collect(Collectors.toList());

        List<BusinessDTO> listByName = businessRepository
                .findByNameStartingWithIgnoreCase(search)
                .stream()
                .map(BusinessMapper::toBusinessDTO)
                .collect(Collectors.toList());


        List<BusinessDTO> combine = Stream.concat(listByUsername.stream(), listByName.stream())
                .collect(Collectors.toList());


        return combine.stream()
                .filter(distinctByKey(BusinessDTO::getId))
                .collect(Collectors.toList());
    }

    @Override
    public List<BusinessDTO> getAllBusinessesByCountry(String country) {
        return businessRepository
                .findByCountryIgnoreCase(country)
                .stream()
                .map(BusinessMapper::toBusinessDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<BusinessDTO> getAllBusinessesByCountryAndRegion(String country, String region) {
        return businessRepository
                .findByCountryAndRegionAllIgnoreCase(country, region)
                .stream()
                .map(BusinessMapper::toBusinessDTO)
                .collect(Collectors.toList());
    }

    @Override
    public BusinessDTO getBusinessById(String id) {
        if (businessRepository.findById(id).isPresent())
            return BusinessMapper.toBusinessDTO(businessRepository.findById(id).get());
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public BusinessDTO createBusiness(BusinessDTO businessDTO) {
        Business business = BusinessMapper.toBusiness(businessDTO);
        return BusinessMapper.toBusinessDTO(businessRepository.save(business));
    }

    @Override
    public BusinessDTO updateBusiness(BusinessDTO businessDTO, String id) {
        if (businessRepository.findById(id).isPresent()) {
            businessDTO.setId(id);
            Business existingBusiness = BusinessMapper.toBusiness(businessDTO);
            return BusinessMapper.toBusinessDTO(businessRepository.save(existingBusiness));
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public BusinessDTO deleteBusinessById(String id) {
        if (businessRepository.findById(id).isPresent()) {
            Business business = businessRepository.findById(id).get();
            businessRepository.delete(business);
            return BusinessMapper.toBusinessDTO(business);
        } else
            throw new MongoException("Record not found");
    }
}
