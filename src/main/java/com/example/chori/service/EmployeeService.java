package com.example.chori.service;

import com.example.chori.dto.EmployeeDTO;
import com.example.chori.model.DateSlots;

import java.util.Date;
import java.util.List;

/**
 * The interface Employee service.
 */
public interface EmployeeService {

    /**
     * Gets all employees.
     *
     * @return the all employees
     */
    List<EmployeeDTO> getAllEmployees();

    /**
     * Gets employee by id.
     *
     * @param id the id
     * @return the employee by id
     */
    EmployeeDTO getEmployeeById(String id);

    /**
     * Create employee employee dto.
     *
     * @param employeeDTO the employee dto
     * @return the employee dto
     */
    EmployeeDTO createEmployee(EmployeeDTO employeeDTO);

    /**
     * Update employee employee dto.
     *
     * @param employeeDTO the employee dto
     * @param id          the id
     * @return the employee dto
     */
    EmployeeDTO updateEmployee(EmployeeDTO employeeDTO, String id);

    /**
     * Delete employee by id employee dto.
     *
     * @param id the id
     * @return the employee dto
     */
    EmployeeDTO deleteEmployeeById(String id);

    /**
     * Gets employees by business.
     *
     * @param businessId the business id
     * @return the employees by business
     */
    List<EmployeeDTO> getEmployeesByBusiness(String businessId);

    /**
     * Gets slots by employee.
     *
     * @param employeeId the employee id
     * @param date       the date
     * @return the slots by employee
     */
    List<DateSlots> getSlotsByEmployee(String employeeId, Date date);

}
