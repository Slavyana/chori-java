package com.example.chori.service;

import com.example.chori.model.Comment;
import com.example.chori.repository.CommentRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

/**
 * The type Comment service.
 */
@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;

    /**
     * Instantiates a new Comment service.
     *
     * @param commentRepository the comment repository
     */
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public Comment getCommentById(String id) {
        if (commentRepository.findById(id).isPresent())
            return commentRepository.findById(id).get();
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public Comment createComment(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public Comment updateComment(Comment comment, String id) {
        if (commentRepository.findById(id).isPresent()) {
            return commentRepository.save(comment);
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public Comment deleteCommentById(String id) {
        if (commentRepository.findById(id).isPresent()) {
            Comment comment = commentRepository.findById(id).get();
            commentRepository.delete(comment);
            return comment;
        } else
            throw new MongoException("Record not found");
    }
}
