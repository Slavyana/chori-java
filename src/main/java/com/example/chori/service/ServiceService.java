package com.example.chori.service;


import com.example.chori.dto.ServiceDTO;

import java.util.List;

/**
 * The interface Service service.
 */
public interface ServiceService {
    /**
     * Gets all services.
     *
     * @return the all services
     */
    List<ServiceDTO> getAllServices();

    /**
     * Gets service by id.
     *
     * @param id the id
     * @return the service by id
     */
    ServiceDTO getServiceById(String id);

    /**
     * Create service service dto.
     *
     * @param serviceDTO the service dto
     * @return the service dto
     */
    ServiceDTO createService(ServiceDTO serviceDTO);

    /**
     * Update service service dto.
     *
     * @param serviceDTO the service dto
     * @param id         the id
     * @return the service dto
     */
    ServiceDTO updateService(ServiceDTO serviceDTO, String id);

    /**
     * Delete service by id service dto.
     *
     * @param id the id
     * @return the service dto
     */
    ServiceDTO deleteServiceById(String id);

    /**
     * Gets services by business.
     *
     * @param businessId the business id
     * @return the services by business
     */
    List<ServiceDTO> getServicesByBusiness(String businessId);
}
