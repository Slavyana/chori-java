package com.example.chori.service;

import com.example.chori.dto.EmployeeDTO;
import com.example.chori.dto.mapper.EmployeeMapper;
import com.example.chori.model.DateSlots;
import com.example.chori.model.Employee;
import com.example.chori.repository.EmployeeRepository;
import com.mongodb.MongoException;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Employee service.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;

    /**
     * Instantiates a new Employee service.
     *
     * @param employeeRepository the employee repository
     */
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<EmployeeDTO> getAllEmployees() {
        return employeeRepository
                .findAll()
                .stream()
                .map(EmployeeMapper::toEmployeeDTO)
                .collect(Collectors.toList());
    }

    @Override
    public EmployeeDTO getEmployeeById(String id) {

        if (employeeRepository.findById(id).isPresent())
            return EmployeeMapper.toEmployeeDTO(employeeRepository.findById(id).get());
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public EmployeeDTO createEmployee(EmployeeDTO employeeDTO) {
        Employee employee = EmployeeMapper.toEmployee(employeeDTO);
        return EmployeeMapper.toEmployeeDTO(employeeRepository.save(employee));
    }

    @Override
    public EmployeeDTO updateEmployee(EmployeeDTO employeeDTO, String id) {
        if (employeeRepository.findById(id).isPresent()) {
            employeeDTO.setId(id);
            Employee existingEmployee = EmployeeMapper.toEmployee(employeeDTO);
            return EmployeeMapper.toEmployeeDTO(employeeRepository.save(existingEmployee));
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public EmployeeDTO deleteEmployeeById(String id) {
        if (employeeRepository.findById(id).isPresent()) {
            Employee employee = employeeRepository.findById(id).get();
            employeeRepository.delete(employee);
            return EmployeeMapper.toEmployeeDTO(employee);
        } else
            throw new MongoException("Record not found");
    }

    @Override
    public List<EmployeeDTO> getEmployeesByBusiness(String businessId) {

        List<EmployeeDTO> employeeList = employeeRepository.findByBusinessId(businessId)
                .stream()
                .map(EmployeeMapper::toEmployeeDTO)
                .collect(Collectors.toList());

        return employeeList;
    }

    @Override
    public List<DateSlots> getSlotsByEmployee(String employeeId, Date date) {
        if (employeeRepository.findById(employeeId).isPresent())

            if (date == null) {
                return employeeRepository.findById(employeeId).get().getDateSlotsList();
            } else {
                SimpleDateFormat dateOnly = new SimpleDateFormat("yyyy-MM-dd");
                List<DateSlots> result = employeeRepository.findById(employeeId).get().getDateSlotsList();
                if(result != null) {
                    return result.stream().filter(d -> dateOnly.format(d.getDate()).equals(dateOnly.format(date))).collect(Collectors.toList());
                } else {
                    return new ArrayList<DateSlots>();
                }
            }

        else
            throw new MongoException("Record not Found");
    }


}
