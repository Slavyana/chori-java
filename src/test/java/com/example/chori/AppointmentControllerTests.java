package com.example.chori;

import com.example.chori.controller.AppointmentController;
import com.example.chori.dto.AppointmentDTO;
import com.example.chori.service.AppointmentService;
import com.example.chori.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(AppointmentController.class)

public class AppointmentControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AppointmentService appointmentService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private com.example.chori.jwt.AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private com.example.chori.jwt.JwtUtils jwtUtils;

    @Test
    public void getAll() throws Exception {
        given(this.appointmentService.getAllAppointments()).willReturn(new ArrayList<>());
        this.mvc.perform(get("/api/appointments")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getById() throws Exception {
        given(this.appointmentService.getAppointmentById("abc")).willReturn(new AppointmentDTO());
        this.mvc.perform(get("/api/appointments/abc"))
                .andExpect(status().isOk());
    }


    @Test
    public void postJSON() throws Exception {

        AppointmentDTO input = new AppointmentDTO();
        input.setId("abc");

        given(this.appointmentService.createAppointment(input)).willReturn(input);
        final ResultActions result =
                mvc.perform(
                        post("/api/appointments")
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated());
    }

    @Test
    public void putJSON() throws Exception {

        AppointmentDTO input = new AppointmentDTO();
        input.setId("abc");

        given(this.appointmentService.updateAppointment(input, "abc")).willReturn(input);

        final ResultActions result =
                mvc.perform(
                        put("/api/appointments/abc")
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());
    }

    @Test
    public void deleteById() throws Exception {
        given(this.appointmentService.deleteAppointmentById("abc")).willReturn(new AppointmentDTO());
        this.mvc.perform(delete("/api/appointments/abc"))
                .andExpect(status().isOk());
    }

}