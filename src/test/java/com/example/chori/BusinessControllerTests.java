package com.example.chori;

import com.example.chori.controller.BusinessController;
import com.example.chori.dto.BusinessDTO;
import com.example.chori.service.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(BusinessController.class)

public class BusinessControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BusinessService businessService;

    @MockBean
    private ServiceService serviceService;

    @MockBean
    private EmployeeService employeeService;

    @MockBean
    private AppointmentService appointmentService;

    @MockBean
    private ProductService productService;

    @MockBean
    private PostService postService;

    @MockBean
    private ReviewService reviewService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private com.example.chori.jwt.AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private com.example.chori.jwt.JwtUtils cjwtUtils;

    private final String URI = "/api/businesses";

    @Test
    public void getAll() throws Exception {
        given(this.businessService.getAllBusinesses()).willReturn(new ArrayList<>());
        this.mvc.perform(get(URI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getById() throws Exception {
        given(this.businessService.getBusinessById("abc")).willReturn(new BusinessDTO());
        this.mvc.perform(get(URI + "/abc"))
                .andExpect(status().isOk());
    }


    @Test
    public void postJSON() throws Exception {

        BusinessDTO input = new BusinessDTO();
        input.setId("abc");

        given(this.businessService.createBusiness(input)).willReturn(input);
        final ResultActions result =
                mvc.perform(
                        post(URI)
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    public void putJSON() throws Exception {

        BusinessDTO input = new BusinessDTO();
        input.setId("abc");

        given(this.businessService.updateBusiness(input, "abc")).willReturn(input);

        final ResultActions result =
                mvc.perform(
                        put(URI + "/abc")
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    public void deleteById() throws Exception {
        given(this.businessService.deleteBusinessById("abc")).willReturn(new BusinessDTO());
        this.mvc.perform(delete(URI + "/abc"))
                .andExpect(status().isOk());
    }

}