package com.example.chori;

import com.example.chori.controller.ProductController;
import com.example.chori.dto.ProductDTO;
import com.example.chori.service.ProductService;
import com.example.chori.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private com.example.chori.jwt.AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private com.example.chori.jwt.JwtUtils jwtUtils;

    private final String URI = "/api/products";

    @Test
    public void getAll() throws Exception {
        given(this.productService.getAllProducts()).willReturn(new ArrayList<>());
        this.mvc.perform(get(URI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getById() throws Exception {
        given(this.productService.getProductById("abc")).willReturn(new ProductDTO());
        this.mvc.perform(get(URI + "/abc"))
                .andExpect(status().isOk());
    }


    @Test
    public void postJSON() throws Exception {

        ProductDTO input = new ProductDTO();
        input.setId("abc");

        given(this.productService.createProduct(input)).willReturn(input);
        final ResultActions result =
                mvc.perform(
                        post(URI)
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated());
    }

    @Test
    public void putJSON() throws Exception {

        ProductDTO input = new ProductDTO();
        input.setId("abc");

        given(this.productService.updateProduct(input, "abc")).willReturn(input);

        final ResultActions result =
                mvc.perform(
                        put(URI + "/abc")
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());
    }

    @Test
    public void deleteById() throws Exception {
        given(this.productService.deleteProductById("abc")).willReturn(new ProductDTO());
        this.mvc.perform(delete(URI + "/abc"))
                .andExpect(status().isOk());
    }
}