package com.example.chori;


import com.example.chori.controller.ReviewController;
import com.example.chori.model.Review;
import com.example.chori.service.ReviewService;
import com.example.chori.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ReviewController.class)
public class ReviewControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReviewService reviewService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private com.example.chori.jwt.AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private com.example.chori.jwt.JwtUtils jwtUtils;

    private final String URI = "/api/reviews";

    @Test
    public void getAll() throws Exception {
        given(this.reviewService.getAllReviews()).willReturn(new ArrayList<>());
        this.mvc.perform(get(URI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getById() throws Exception {
        given(this.reviewService.getReviewById("abc")).willReturn(new Review());
        this.mvc.perform(get(URI + "/abc"))
                .andExpect(status().isOk());
    }


    @Test
    public void postJSON() throws Exception {

        Review input = new Review();
        input.setId("abc");

        given(this.reviewService.createReview(input)).willReturn(input);
        final ResultActions result =
                mvc.perform(
                        post(URI)
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated());
    }

    @Test
    public void putJSON() throws Exception {

        Review input = new Review();
        input.setId("abc");

        given(this.reviewService.updateReview(input, "abc")).willReturn(input);

        final ResultActions result =
                mvc.perform(
                        put(URI + "/abc")
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());
    }

    @Test
    public void deleteById() throws Exception {
        given(this.reviewService.deleteReviewById("abc")).willReturn(new Review());
        this.mvc.perform(delete(URI + "/abc"))
                .andExpect(status().isOk());
    }
}