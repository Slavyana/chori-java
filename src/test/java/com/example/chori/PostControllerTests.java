package com.example.chori;

import com.example.chori.controller.PostController;
import com.example.chori.model.Post;
import com.example.chori.service.CommentService;
import com.example.chori.service.PostService;
import com.example.chori.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PostController.class)
public class PostControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PostService postService;

    @MockBean
    private CommentService commentService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private com.example.chori.jwt.AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private com.example.chori.jwt.JwtUtils jwtUtils;

    private final String URI = "/api/posts";

    @Test
    public void getAll() throws Exception {
        given(this.postService.getAllPosts()).willReturn(new ArrayList<>());
        this.mvc.perform(get(URI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getById() throws Exception {
        given(this.postService.getPostById("abc")).willReturn(new Post());
        this.mvc.perform(get(URI + "/abc"))
                .andExpect(status().isOk());
    }


    @Test
    public void postJSON() throws Exception {

        Post input = new Post();
        input.setId("abc");

        given(this.postService.createPost(input)).willReturn(input);
        final ResultActions result =
                mvc.perform(
                        post(URI)
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated());
    }

    @Test
    public void putJSON() throws Exception {

        Post input = new Post();
        input.setId("abc");

        given(this.postService.updatePost(input, "abc")).willReturn(input);

        final ResultActions result =
                mvc.perform(
                        put(URI + "/abc")
                                .content("{ \"id\": \"abc\" }")
                                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());
    }

    @Test
    public void deleteById() throws Exception {
        given(this.postService.deletePostById("abc")).willReturn(new Post());
        this.mvc.perform(delete(URI + "/abc"))
                .andExpect(status().isOk());
    }

}