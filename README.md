# Getting Started with the Appointment Booking API

## OpenJDK
This project was built using OpenJDK 14: the open-source implementation of the Java Platform, Standard Edition.

See http://openjdk.java.net/ for more information about the OpenJDK Community and installation guides.

## Spring boot
The api utilises tools from the [Spring Boot framework](https://spring.io/projects/spring-boot).

## Gradle
See Gradle's [Installation Guide](https://docs.gradle.org/current/userguide/installation.html).

## IntelliJ IDEA
For convenience, you can import the project using [IntelliJ IDEA](https://www.jetbrains.com/help/idea/gradle.html).
